import inView from './web_modules/in-view.js';
import $ from './web_modules/jquery.js';
import gsap from './web_modules/gsap.js';
import ScrollTrigger from './web_modules/gsap/ScrollTrigger.js';

gsap.registerPlugin(ScrollTrigger);

$(document).ready(function() {

	// Animation on mobile only
	if (window.innerWidth >= 480) {
		gsap.utils.toArray(".section").forEach((panel, i) => {
		  ScrollTrigger.create({
		    trigger: panel,
		    start: "top top", 
		    pin: true, 
		    pinSpacing: false 
		  });
		});


		ScrollTrigger.create({
		  snap: 1 / 4 // snap whole page to the closest section!
		});
	}

	// show sidebar outside hero section
	inView('#hero').on('enter', function() {
		$('#side-nav').addClass('hide');
		$('#side-nav').removeClass('white');

		$('a.attend-btn').addClass('hide');
		$('img.logo').attr('src', './images/logo-white-all.svg');
		console.log('in hero')
	}).on('exit', function(){
		console.log('out hero')
	});

	inView.offset(window.innerHeight/2);

	// change color of logo and sidebar fonts outside about section
	inView('#about').on('enter', function() {
		$('#side-nav').removeClass('hide');
		$('a.attend-btn').removeClass('hide');

		$('#side-nav').removeClass('white');
		$('a.attend-btn').addClass('blue');

		$('img.icon-burger-img').attr('src','./images/burger-icon-blue.svg');
		$('img.logo').attr('src', './images/logo-blue.svg');
		$('a.link-about').addClass('active');
		console.log('in about')
	}).on('exit', function(){
		console.log('out about')
		$('a.link-why').removeClass('active');
	});

	// switch active link for the section in view
	inView('#why').on('enter', function() {
		$('#side-nav').addClass('white');
		$('a.attend-btn').removeClass('blue');
		$('img.logo').attr('src', './images/logo-white-all.svg');
		$('a.link-about').removeClass('active');
		$('img.icon-burger-img').attr('src','./images/burger-icon.svg');
		$('a.link-why').addClass('active');
		console.log('in why')
	}).on('exit', function(){
		$('a.link-about').removeClass('active');
		$('a.link-speakers').removeClass('active');
		console.log('out why')
	});
	inView('#speakers').on('enter', function() {
		$('a.link-speakers').addClass('active');
	}).on('exit', function(){
		
		$('a.link-why').removeClass('active');
	});
	inView('#agenda').on('enter', function() {
		$('a.link-agenda').addClass('active');
	}).on('exit', function(){
		$('a.link-speakers').removeClass('active');
		$('a.link-agenda').removeClass('active');
	});


	// responsive agenda collapse functionality
	// click on icon
	$('a.collapse-agenda').on('click', function(ev) {
		ev.preventDefault();
		var $parent = $(this).parent();
		var $info = $parent.siblings('.info');
		if ($parent.hasClass('flipped')){
			// un-collapse
			$parent.removeClass('flipped');
			$info.children('.description').addClass('hide');

		} else {
			// collapse
			$parent.addClass('flipped');
			$info.children('.description').removeClass('hide');
		}
	});

	// responsive agenda collapse functionality
	// click on title
	$('a.collapse-agenda-title').on('click', function(ev) {
		ev.preventDefault();
		var $parent = $(this).parent().parent();
		var $icon = $parent.siblings('.icon-collapse');
		var $description = $(this).parent().siblings('.description');

		if($icon.hasClass('flipped')) {
			$icon.removeClass('flipped');
			$description.addClass('hide');
		} else {
			$icon.addClass('flipped');
			$description.removeClass('hide');
		}
	});

	// responsive menu
	// open
	$('a.burger-icon').on('click', function(ev) {
		ev.preventDefault();
		$('.expand-menu').removeClass('hide');
	});

	// responsive menu
	// close
	$('a.burger-icon-close').on('click', function(ev) {
		ev.preventDefault();
		$('.expand-menu').addClass('hide');
	});

	$('a.mob-menu-link').on('click', function(ev) {
		$('.expand-menu').addClass('hide');
	});

});

